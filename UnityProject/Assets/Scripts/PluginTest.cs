using UnityEngine;

public class PluginTest : MonoBehaviour
{
    /* This static function will run when the game/application starts. */
    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    private static void OnLoad()
    {
        Debug.Log($"get_number: {NativePluginInterface.get_number().ToString()}");
        Debug.Log($"is_windows: {NativePluginInterface.is_windows().ToString()}");
    }
}

