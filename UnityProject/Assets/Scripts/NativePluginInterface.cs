using System.Runtime.InteropServices;

/* C# interface for our native plugin. */
public class NativePluginInterface
{
    [DllImport("NativePlugin")]
    public static extern int get_number();

    [DllImport("NativePlugin")]
    public static extern bool is_windows();
}

