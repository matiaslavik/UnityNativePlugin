@echo off

set CMAKE_PATH="D:\Program Files\CMake\bin\cmake.exe"
set ANDROID_NDK_PATH=D:\UnityAndroidNDK\NDK

rmdir /s /q build
mkdir build
cd build

%CMAKE_PATH% .. ^
  -G"MinGW Makefiles" ^
  -DCMAKE_BUILD_TYPE=Release ^
  -DCMAKE_TOOLCHAIN_FILE=%ANDROID_NDK_PATH%\build\cmake\android.toolchain.cmake ^
  -DCMAKE_MAKE_PROGRAM=%ANDROID_NDK_PATH%\prebuilt\windows-x86_64\bin\make.exe ^
  -DANDROID_NDK=%ANDROID_NDK_PATH% ^
  -DANDROID_NATIVE_API_LEVEL=android-9 ^
  -DANDROID_ABI=armeabi-v7a

%CMAKE_PATH% --build .

popd
