ANDROID_NDK_PATH="/home/matias/Unity/Hub/Editor/2020.2.1f1/Editor/Data/PlaybackEngines/AndroidPlayer/NDK"
cmake . \
  -G"Unix Makefiles" \
  -DCMAKE_BUILD_TYPE=Release \
  -DCMAKE_TOOLCHAIN_FILE="${ANDROID_NDK_PATH}/build/cmake/android.toolchain.cmake" \
  -DCMAKE_MAKE_PROGRAM="${ANDROID_NDK_PATH}/prebuilt/linux-x86_64/bin/make" \
  -DANDROID_NDK="${ANDROID_NDK_PATH}" \
  -DANDROID_NATIVE_API_LEVEL=android-9 \
  -DANDROID_ABI=arm64-v8a
  
cmake --build .
